/*
Given the following code:

function test() {
    return job().then(function() {
        return job2().then(function() {
            return job3().then(function() {
                return job4().then(function() {
                    doSomething();
                });
            });
        });
    });
}

function job1() {
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            resolve('result of job 1');
        }, 1000);
    });
}

function job2() {
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            resolve('result of job 2');
        }, 1000);
    });
}

function job3() {
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            resolve('result of job 3');
        }, 1000);
    });
}

function job4() {
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            resolve('result of job 4');
        }, 1000);
    });
}


The problem: Pyramid of Doom

Use best practices to avoid this problem and reach the following result:

Output example:

```
Value is... result of job 4
```

*/



//+++ YOUR CODE GOES HERE


//test()



//doSomething()




//job1()




//job2()




//job3()




//job4()







// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
// *~*~*~*~*~*~*~* Tests (Don't Touch) *~*~*~*~*~*~*~*~*
// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*


//test promise
test(doSomething)


/* Output

Value is... result of job 4

*/















/*source:

- Adapted from https://www.codingame.com/playgrounds/347/javascript-promises-mastering-the-asynchronous/traps-of-promises


*/



